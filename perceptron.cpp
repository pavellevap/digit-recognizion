#include "perceptron.h"

/**
=======================================================================================================
                                    Элементарный перцептрон
=======================================================================================================
*/

void SaveElementaryPerceptron(const ElementaryPerceptronData& pd, string FileName) {
    IO io;
    io.openOF(FileName.c_str());
    io.writet(pd.amountOfS, sizeof(pd.amountOfS) * 8);
    io.writet(pd.amountOfA, sizeof(pd.amountOfA) * 8);
    io.writet(pd.amountOfR, sizeof(pd.amountOfR) * 8);
    for (size_t i = 0; i < pd.amountOfA; i++)
        for (size_t j = 0; j < pd.amountOfS; j++)
            if (pd.ASEdges[i][j] == 1)
                io.writebit(1);
            else
                io.writebit(0);
    for (size_t i = 0; i < pd.amountOfR; i++)
        for (size_t j = 0; j < pd.amountOfA; j++)
            io.writet(pd.RAEdges[i][j], sizeof(pd.RAEdges[i][j]) * 8);
    io.closeOF();
}

void LoadElementaryPerceptron(ElementaryPerceptronData& pd, string FileName) {
    IO io;
    io.openIF(FileName.c_str());
    io.readt(pd.amountOfS, sizeof(pd.amountOfS) * 8);
    io.readt(pd.amountOfA, sizeof(pd.amountOfA) * 8);
    io.readt(pd.amountOfR, sizeof(pd.amountOfR) * 8);

    pd.ASEdges = new short*[pd.amountOfA];
    for (size_t i = 0; i < pd.amountOfA; i++) {
        pd.ASEdges[i] = new short[pd.amountOfS];
        for (size_t j = 0; j < pd.amountOfS; j++) {
            uchar bit;
            io.readbit(bit);
            if (bit)
                pd.ASEdges[i][j] = 1;
            else
                pd.ASEdges[i][j] = -1;
        }
    }
    pd.RAEdges = new short*[pd.amountOfR];
    for (size_t i = 0; i < pd.amountOfR; i++) {
        pd.RAEdges[i] = new short[pd.amountOfA];
        for (size_t j = 0; j < pd.amountOfA; j++)
            io.readt(pd.RAEdges[i][j], sizeof(pd.RAEdges[i][j]) * 8);
    }
    io.closeIF();
}

ElementaryPerceptronData::ElementaryPerceptronData() {
    initialize();
}

ElementaryPerceptronData::ElementaryPerceptronData(const ElementaryPerceptronData& pd) {
    initialize();
    initialize(pd);
}

ElementaryPerceptronData::~ElementaryPerceptronData() {
    destroy();
}

void ElementaryPerceptronData::initialize() {
    amountOfS = amountOfA = amountOfR = 0;
    RAEdges = NULL;
    ASEdges = NULL;
}

void ElementaryPerceptronData::initialize(const ElementaryPerceptronData& pd) {
    destroy();

    amountOfS = pd.amountOfS;
	amountOfA = pd.amountOfA;
    amountOfR = pd.amountOfR;

    ASEdges = new short* [amountOfA];
    for (size_t i = 0; i < amountOfA; i++) {
        ASEdges[i] = new short[amountOfS];
        memcpy(ASEdges[i], pd.ASEdges[i], amountOfS * sizeof(short));
    }

    RAEdges = new short* [amountOfR];
    for (size_t i = 0; i < amountOfR; i++) {
        RAEdges[i] = new short[amountOfA];
        memcpy(RAEdges[i], pd.RAEdges[i], amountOfA * sizeof(short));
    }
}

void ElementaryPerceptronData::destroy() {
    if (ASEdges) {
        for (size_t i = 0; i < amountOfA; i++)
            if (ASEdges[i]) {
                delete[] ASEdges[i];
                ASEdges[i] = NULL;
            }
        delete[] ASEdges;
        ASEdges = NULL;
    }

    if (RAEdges) {
        for (size_t i = 0; i < amountOfR; i++)
            if (RAEdges[i]) {
                delete[] RAEdges[i];
                RAEdges[i] = NULL;
            }
        delete[] RAEdges;
        RAEdges = NULL;
    }
}

ElementaryPerceptronData& ElementaryPerceptronData::operator = (const ElementaryPerceptronData& pd) {
    initialize(pd);
    return *this;
}

ElementaryPerceptron::ElementaryPerceptron() {
    initialize();
}

ElementaryPerceptron::ElementaryPerceptron(const ElementaryPerceptron& p) {
    initialize();
    initialize(p);
}

ElementaryPerceptron::ElementaryPerceptron(size_t amountOfS, size_t amountOfA, size_t amountOfR) {
    initialize();
    initialize(amountOfS, amountOfA, amountOfR);
}

ElementaryPerceptron::~ElementaryPerceptron() {
    destroy();
}

void ElementaryPerceptron::initialize() {
    amountOfS = amountOfA = amountOfR = 0;
    AOutput = NULL;
    ROutput = NULL;
    input = NULL;
    ASEdges = NULL;
    RAEdges = NULL;
}

void ElementaryPerceptron::initialize(const ElementaryPerceptron& p) {
    destroy();

    amountOfS = p.amountOfS;
	amountOfA = p.amountOfA;
    amountOfR = p.amountOfR;

    AOutput = new bool[amountOfA];
    memcpy(AOutput, p.AOutput, amountOfA * sizeof(bool));

    ROutput = new int[amountOfR];
    memcpy(ROutput, p.ROutput, amountOfR * sizeof(int));

    input = new bool[amountOfS];
    memcpy(input, p.input, amountOfS * sizeof(bool));

    ASEdges = new short*[amountOfA];
    for (size_t i = 0; i < amountOfA; i++) {
        ASEdges[i] = new short[amountOfS];
        memcpy(ASEdges[i], p.ASEdges[i], amountOfS * sizeof(short));
    }

    RAEdges = new short*[amountOfR];
    for (size_t i = 0; i < amountOfR; i++) {
        RAEdges[i] = new short[amountOfA];
        memcpy(RAEdges[i], p.RAEdges[i], amountOfA * sizeof(short));
    }
}

void ElementaryPerceptron::initialize(size_t amountOfS, size_t amountOfA, size_t amountOfR){
    destroy();

	this->amountOfS = amountOfS;
	this->amountOfA = amountOfA;
    this->amountOfR = amountOfR;

    AOutput = new bool[amountOfA];
    ROutput = new int[amountOfR];
    input = new bool[amountOfS];

    ASEdges = new short*[amountOfA];
    for (size_t i = 0; i < amountOfA; i++) {
        ASEdges[i] = new short[amountOfS];

        for (size_t j = 0; j < amountOfS; j++)
            ASEdges[i][j] = (rand() & 1) ? 1 : -1;
    }

    RAEdges = new short*[amountOfR];
    for (size_t i = 0; i < amountOfR; i++) {
        RAEdges[i] = new short[amountOfA];
        memset(RAEdges[i], 0, sizeof(short) * amountOfA);
    }
}

void ElementaryPerceptron::destroy() {
    if (ASEdges) {
        for (size_t i = 0; i < amountOfA; i++)
            if (ASEdges[i]) {
                delete[] ASEdges[i];
                ASEdges[i] = NULL;
            }
        delete[] ASEdges;
        ASEdges = NULL;
    }

    if (RAEdges) {
        for (size_t i = 0; i < amountOfR; i++)
            if (RAEdges[i]) {
                delete[] RAEdges[i];
                RAEdges[i] = NULL;
            }
        delete[] RAEdges;
        RAEdges = NULL;
    }

    if (input) {
        delete[] input;
        input = NULL;
    }

    if (AOutput) {
        delete[] AOutput;
        AOutput = NULL;
    }

    if (ROutput) {
        delete[] ROutput;
        ROutput = NULL;
    }
}

ElementaryPerceptron& ElementaryPerceptron::operator = (const ElementaryPerceptron& p) {
    initialize(p);
    return *this;
}

void ElementaryPerceptron::restoreElementaryPerceptron(const ElementaryPerceptronData& pd) {
    destroy();

    amountOfR = pd.amountOfR;
    amountOfA = pd.amountOfA;
    amountOfS = pd.amountOfS;

    ASEdges = new short*[amountOfA];
    for (size_t i = 0; i < amountOfA; i++) {
        ASEdges[i] = new short[amountOfS];
        memcpy(ASEdges[i], pd.ASEdges[i], sizeof(short) * amountOfS);
    }

    RAEdges = new short*[amountOfR];
    for (size_t i = 0; i < amountOfR; i++) {
        RAEdges[i] = new short[amountOfA];
        memcpy(RAEdges[i], pd.RAEdges[i], sizeof(short) * amountOfA);
    }

    AOutput = new bool[amountOfA];
    ROutput = new int[amountOfR];

    input = new bool[amountOfS];
}

ElementaryPerceptronData ElementaryPerceptron::getElementaryPerceptronData() const {
    ElementaryPerceptronData pd;
    pd.amountOfR = amountOfR;
    pd.amountOfA = amountOfA;
    pd.amountOfS = amountOfS;

    pd.ASEdges = new short*[amountOfA];
    for (size_t i = 0; i < amountOfA; i++) {
        pd.ASEdges[i] = new short[amountOfS];
        memcpy(pd.ASEdges[i], ASEdges[i], sizeof(short) * amountOfS);
    }

    pd.RAEdges = new short*[amountOfR];
    for (size_t i = 0; i < amountOfR; i++) {
        pd.RAEdges[i] = new short[amountOfA];
        memcpy(pd.RAEdges[i], RAEdges[i], sizeof(short) * amountOfA);
    }

    return pd;
}

void ElementaryPerceptron::setInput(bool* in) {
    memcpy(input, in, amountOfS * sizeof(bool));
}

void ElementaryPerceptron::setInput(size_t index, bool value) {
    if (index >= amountOfS)
        cerr << "Выход за границу массива в функции ElementaryPerceptron::setInput()\n";
    input[index] = value;
}

void ElementaryPerceptron::setAOutput(bool* out) {
    memcpy(AOutput, out, amountOfA * sizeof(bool));
}

void ElementaryPerceptron::setAOutput(size_t index, bool value) {
    if (index >= amountOfA)
        cerr << "Выход за границу массива в функции ElementaryPerceptron::setAOutput()\n";
    AOutput[index] = value;
}

void ElementaryPerceptron::calculateAOutput() {
    #pragma omp parallel for
    for (size_t i = 0; i < amountOfA; ++i) {
        int sum = 0;
        for (size_t j = 0; j < amountOfS; ++j)
            if (input[j])
                sum += ASEdges[i][j];

        AOutput[i] = sum > 0;                        /// Пороговое значение - 0
    }
}

void ElementaryPerceptron::calculateROutput() {
    #pragma omp parallel for
    for (size_t i = 0; i < amountOfR; ++i) {
        ROutput[i] = 0;

        for (size_t j = 0; j < amountOfA; ++j)
            if (AOutput[j])
                ROutput[i] += RAEdges[i][j];
    }
}

void ElementaryPerceptron::calculateOutput() {
    calculateAOutput();
    calculateROutput();
}

void ElementaryPerceptron::correct(size_t index, int add) {
    if (index >= amountOfR)
        cerr << "Выход за границы массива в функции ElementaryPerceptron::correct()\n";

    #pragma omp parallel for
    for (size_t i = 0; i < amountOfA; i++)
        if (AOutput[i])
            RAEdges[index][i] += add;
}

void ElementaryPerceptron::teach(int* desierdOutput) {
    for (size_t i = 0; i < amountOfR; i++) {
        bool ans1 = ROutput[i] > 0;
        bool ans2 = desierdOutput[i] > 0;
        if (ans1 != ans2) {
            if (ROutput[i] > 0)
                correct(i, -1);
            else
                correct(i, 1);
        }
    }
}

bool* ElementaryPerceptron::getAOutput() const {
    bool* output = new bool[amountOfA];
    memcpy(output, AOutput, sizeof(bool) * amountOfA);
    return output;
}

bool ElementaryPerceptron::getAOutput(size_t index) const {
    if (index >= amountOfA)
        cerr << "Выход за границу массива в функции ElementaryPerceptron::getAOutput()\n";
    return AOutput[index];
}

int* ElementaryPerceptron::getROutput() const {
    int* output = new int[amountOfR];
    memcpy(output, ROutput, sizeof(int) * amountOfR);
    return output;
}

int ElementaryPerceptron::getROutput(size_t index) const {
    if (index >= amountOfR)
        cerr << "Выход за границу массива в функции ElementaryPerceptron::getROutput()\n";
    return ROutput[index];
}

size_t ElementaryPerceptron::getAmountOfR() const {
    return amountOfR;
}

size_t ElementaryPerceptron::getAmountOfA() const {
    return amountOfA;
}

size_t ElementaryPerceptron::getAmountOfS() const {
    return amountOfS;
}

/**
=======================================================================================================
                                    Многослойный перцептрон
=======================================================================================================
*/

inline float dotProduct(const size_t N, float *X, float *Y) {
    return dotProductSSE(X, Y, N);

//    float ans = 0;
//    for (int i = 0; i < N; i++, X++, Y++)
//        ans += (*X) * (*Y);
//    return ans;
}

void axpy(const size_t N, const float *X, float* Y, float a) {
    #pragma omp parallel for
    for (int i = 0; i < N; ++i)
        Y[i] += X[i] * a;
}

void SaveMultiLayerPerceptron(const MultiLayerPerceptronData& pd, string FileName) {

    IO io;
    io.openOF(FileName);

    io.writet(pd.amountOfLayers, sizeof(pd.amountOfLayers) * 8);
    for (size_t i = 0; i < pd.amountOfLayers; i++)
        io.writet(pd.actFuncType[i]);
    for (size_t i = 0; i < pd.amountOfLayers; i++) {
        io.writet(pd.layers[i].amountOfInputNeurons, sizeof(pd.layers[i].amountOfInputNeurons) * 8);
        io.writet(pd.layers[i].amountOfOutputNeurons, sizeof(pd.layers[i].amountOfOutputNeurons) * 8);
        for (size_t index = 0; index < pd.layers[i].amountOfOutputNeurons * pd.layers[i].amountOfInputNeurons; index++)
            io.writet(pd.layers[i].weights[index]);
    }

    io.closeOF();

}

void LoadMultiLayerPerceptron(MultiLayerPerceptronData& pd, string FileName) {
    IO io;
    io.openIF(FileName);

    io.readt(pd.amountOfLayers, sizeof(pd.amountOfLayers) * 8);
    pd.layers = new LayerData[pd.amountOfLayers];
    pd.actFuncType = new char[pd.amountOfLayers];
    for (size_t i = 0; i < pd.amountOfLayers; i++)
        io.readt(pd.actFuncType[i]);
    for (size_t i = 0; i < pd.amountOfLayers; i++) {
        io.readt(pd.layers[i].amountOfInputNeurons, sizeof(pd.layers[i].amountOfInputNeurons) * 8);
        io.readt(pd.layers[i].amountOfOutputNeurons, sizeof(pd.layers[i].amountOfOutputNeurons) * 8);

        pd.layers[i].weights = new float[pd.layers[i].amountOfOutputNeurons * pd.layers[i].amountOfInputNeurons];
        for (size_t index = 0; index < pd.layers[i].amountOfOutputNeurons * pd.layers[i].amountOfInputNeurons; index++)
            io.readt(pd.layers[i].weights[index]);
    }

    io.closeIF();
}

LayerData::LayerData() {
    initialize();
}

LayerData::LayerData(const LayerData& ld) {
    initialize();
    initialize(ld);
}

LayerData::~LayerData() {
    destroy();
}

void LayerData::initialize() {
    amountOfInputNeurons = amountOfOutputNeurons = 0;
    weights = NULL;
}

void LayerData::initialize(const LayerData& ld) {
    destroy();

    amountOfInputNeurons = ld.amountOfInputNeurons;
    amountOfOutputNeurons = ld.amountOfOutputNeurons;
    weights = new float[amountOfOutputNeurons * amountOfInputNeurons];
    memcpy(weights, ld.weights, amountOfInputNeurons * amountOfOutputNeurons * sizeof(float));
}

void LayerData::destroy() {
    if (weights) {
        delete[] weights;
        weights = NULL;
    }
}

LayerData& LayerData::operator = (const LayerData& ld) {
    initialize(ld);
    return *this;
}

MultiLayerPerceptronData::MultiLayerPerceptronData() {
    initialize();
}

MultiLayerPerceptronData::MultiLayerPerceptronData(const MultiLayerPerceptronData& pd) {
    initialize();
    initialize(pd);
}

MultiLayerPerceptronData::~MultiLayerPerceptronData() {
    destroy();
}

void MultiLayerPerceptronData::initialize() {
    amountOfLayers = 0;
    layers = NULL;
    actFuncType = NULL;
}

void MultiLayerPerceptronData::initialize(const MultiLayerPerceptronData& pd) {
    destroy();

    amountOfLayers = pd.amountOfLayers;
    layers = new LayerData[amountOfLayers];
    actFuncType = new char[amountOfLayers];
    for (size_t i = 0; i < amountOfLayers; i++)
        layers[i] = pd.layers[i];
    memcpy(actFuncType, pd.actFuncType, amountOfLayers * sizeof(char));
}

void MultiLayerPerceptronData::destroy() {
    if (layers) {
        delete[] layers;
        layers = NULL;
    }
    if (actFuncType) {
        delete[] actFuncType;
        actFuncType = NULL;
    }
}

MultiLayerPerceptronData& MultiLayerPerceptronData::operator = (const MultiLayerPerceptronData& pd) {
    initialize(pd);
    return *this;
}

Layer::Layer() {
    initialize();
}

Layer::Layer(const Layer& l) {
    initialize();
    initialize(l);
}

Layer::Layer(size_t amountOfInputs, size_t amountOfOutputs) {
    initialize();
    initialize(amountOfInputs, amountOfOutputs);
}

Layer::~Layer() {
    destroy();
}

void Layer::initialize() {
    amountOfOutputNeurons = amountOfInputNeurons = 0;
    weights = NULL;
    output = NULL;
    sum = NULL;
    errors = NULL;
}

void Layer::initialize(size_t amountOfInputs, size_t amountOfOutputs) {
    destroy();

    amountOfOutputNeurons = amountOfOutputs;
    amountOfInputNeurons = amountOfInputs;

    weights = new float[amountOfOutputNeurons * amountOfInputNeurons];
    for (size_t index = 0; index < amountOfOutputNeurons * amountOfInputNeurons; index++)
        weights[index] = 1.0 * rand() / RAND_MAX - 0.5;

    output = new float[amountOfOutputNeurons];
    sum = new float[amountOfOutputNeurons];
    errors = new float[amountOfOutputNeurons];
}

void Layer::initialize(const Layer& l) {
    destroy();

    amountOfInputNeurons = l.amountOfInputNeurons;
    amountOfOutputNeurons = l.amountOfOutputNeurons;
    weights = new float[amountOfOutputNeurons * amountOfInputNeurons];
    memcpy(weights, l.weights, amountOfInputNeurons * amountOfOutputNeurons * sizeof(float));

    output = new float[amountOfOutputNeurons];
    memcpy(output, l.output, amountOfOutputNeurons * sizeof(float));

    sum = new float[amountOfOutputNeurons];
    memcpy(sum, l.sum, amountOfOutputNeurons * sizeof(float));

    errors = new float[amountOfOutputNeurons];
    memcpy(errors, l.errors, amountOfOutputNeurons * sizeof(float));
}

void Layer::destroy() {
    if (weights) {
        delete[] weights;
        weights = NULL;
    }
    if (output) {
        delete[] output;
        output = NULL;
    }

    if (sum) {
        delete[] sum;
        sum = NULL;
    }

    if (errors) {
        delete[] errors;
        errors = NULL;
    }
}

Layer& Layer::operator = (const Layer& l) {
    initialize(l);
    return *this;
}

void Layer::restoreLayer(const LayerData& ld) {
    amountOfOutputNeurons = ld.amountOfOutputNeurons;
    amountOfInputNeurons = ld.amountOfInputNeurons;

    weights = new float[amountOfOutputNeurons * amountOfInputNeurons];
    memcpy(weights, ld.weights, amountOfInputNeurons * amountOfOutputNeurons * sizeof(float));

    output = new float[amountOfOutputNeurons];
    sum = new float[amountOfOutputNeurons];
    errors = new float[amountOfOutputNeurons];
}

LayerData Layer::getLayerData() const {
    LayerData ld;
    ld.amountOfInputNeurons = amountOfInputNeurons;
    ld.amountOfOutputNeurons = amountOfOutputNeurons;
    ld.weights = new float[amountOfOutputNeurons * amountOfInputNeurons];
    memcpy(ld.weights, weights, amountOfInputNeurons * amountOfOutputNeurons * sizeof(float));
    return ld;
}

Direction::Direction() {
    initialize();
}

Direction::Direction(const Direction& t) {
    initialize();
    initialize(t);
}

Direction::Direction(size_t size) {
    initialize();
    initialize(size);
}

Direction::~Direction() {
    destroy();
}

Direction& Direction::operator = (const Direction& t) {
    initialize(t);
    return *this;
}

void Direction::initialize() {
    size = 0;
    direction = NULL;
}

void Direction::initialize(size_t size) {
    destroy();

    this->size = size;
    direction = (float*)calloc(size, sizeof(float));
}

void Direction::initialize(const Direction& t) {
    destroy();

    size = t.size;
    direction = (float*)malloc(size * sizeof(float));
    memcpy(direction, t.direction, size * sizeof(float));
}

void Direction::destroy() {
    if (direction) {
        delete[] direction;
        direction = NULL;
    }
    size = 0;
}

float& Direction::operator[] (size_t index)  {
    if (index >= size)
        cerr << "Выход за границу массива\n";
    return direction[index];
}

float Direction::operator[] (size_t index) const {
    if (index >= size)
        cerr << "Выход за границу массива\n";
    return direction[index];
}

float operator * (const Direction& dir1, const Direction& dir2) {
    if (dir1.size != dir2.size) {
        cerr << "Размеры векторов не совпадают\n";
        return 0;
    }

    return dotProduct(dir1.size, dir1.direction, dir2.direction);
}

Direction operator + (const Direction& dir1, const Direction& dir2) {
    if (dir1.size != dir2.size) {
        cerr << "Размеры векторов не совпадают\n";
        return Direction();
    }

    Direction ans(dir1.size);
    for (size_t i = 0; i < dir1.size; i++)
        ans[i] = dir1[i] + dir2[i];

    return ans;
}

Direction operator - (const Direction& dir1, const Direction& dir2) {
    if (dir1.size != dir2.size) {
        cerr << "Размеры векторов не совпадают\n";
        return Direction();
    }

    Direction ans(dir1.size);
    for (size_t i = 0; i < dir1.size; i++)
        ans[i] = dir1[i] - dir2[i];

    return ans;
}

Direction operator * (const Direction& dir, float x) {
    Direction ans(dir.size);
    for (size_t i = 0; i < dir.size; i++)
        ans[i] = dir[i] * x;

    return ans;
}

Direction operator * (float x, const Direction& dir) {
    Direction ans(dir.size);
    for (size_t i = 0; i < dir.size; i++)
        ans[i] = dir[i] * x;

    return ans;
}

Direction operator / (const Direction& dir, float x) {
    Direction ans(dir.size);
    for (size_t i = 0; i < dir.size; i++)
        ans[i] = dir[i] / x;

    return ans;
}

Direction& operator += (Direction& dir1, const Direction& dir2) {
    if (dir1.size != dir2.size) {
        cerr << "Размеры векторов не совпадают\n";
        return dir1;
    }

    for (size_t i = 0; i < dir1.size; i++)
        dir1[i] += dir2[i];

    return dir1;
}

Direction& operator -= (Direction& dir1, const Direction& dir2) {
    if (dir1.size != dir2.size) {
        cerr << "Размеры векторов не совпадают\n";
        return dir1;
    }

    for (size_t i = 0; i < dir1.size; i++)
        dir1[i] -= dir2[i];

    return dir1;
}

Direction& operator *= (Direction& dir, float x) {
    for (size_t i = 0; i < dir.size; i++)
        dir[i] *= x;

    return dir;
}

Direction& operator /= (Direction& dir, float x) {
    for (size_t i = 0; i < dir.size; i++)
        dir[i] /= x;

    return dir;
}

float Direction::length() {
    return sqrt(dotProduct(size, direction, direction));
}

void Direction::normalize() {
    float len = length();
    for (size_t i = 0; i < size; i++)
        direction[i] /= len;
}

void Direction::randomDirection() {
    for (size_t i = 0; i < size; i++)
        direction[i] = rand() / RAND_MAX - 0.5;
}

MultiLayerPerceptron::MultiLayerPerceptron() {
    initialize();
}

MultiLayerPerceptron::MultiLayerPerceptron(const MultiLayerPerceptron& p) {
    initialize();
    initialize(p);
}

MultiLayerPerceptron::MultiLayerPerceptron(size_t amountOfS, size_t amountOfR, size_t amountOfA) {
    initialize();
    initialize(amountOfS, amountOfR, 1, &amountOfA);
}

MultiLayerPerceptron::MultiLayerPerceptron(size_t amountOfS, size_t amountOfR,
                                           size_t amountOfAInFirstLayer, size_t  amountOfAInSecondLayer) {
    initialize();

    size_t sizes[2];
    sizes[0] = amountOfAInFirstLayer;
    sizes[1] = amountOfAInSecondLayer;
    initialize(amountOfS, amountOfR, 2, sizes);
}

MultiLayerPerceptron::MultiLayerPerceptron(size_t amountOfS, size_t amountOfR,
                                           size_t amountOfHiddenLayers, size_t sizesOfLayers[]) {
    initialize();

    if (amountOfHiddenLayers == 0)
        cerr << "Колличество скрытых слоев должно быть больше нуля\n";
    initialize(amountOfS, amountOfR, amountOfHiddenLayers, sizesOfLayers);
}

MultiLayerPerceptron::~MultiLayerPerceptron() {
    destroy();
}

void MultiLayerPerceptron::initialize() {
    amountOfLayers = amountOfS = amountOfR = 0;
    input = NULL;
    layers = NULL;
    actFuncType = NULL;
}

void MultiLayerPerceptron::initialize(const MultiLayerPerceptron& p) {
    destroy();

    amountOfLayers = p.amountOfLayers;
    amountOfS = p.amountOfS;
    amountOfR = p.amountOfR;

    input = new float[amountOfS];
    memcpy(input, p.input, amountOfS * sizeof(float));

    layers = new Layer[amountOfLayers];
    for (size_t i = 0; i < amountOfLayers; i++)
        layers[i] = p.layers[i];

    actFuncType = new char[amountOfLayers];
    memcpy(actFuncType, p.actFuncType, amountOfLayers * sizeof(char));
}

void MultiLayerPerceptron::initialize(size_t amountOfS, size_t amountOfR,
                                      size_t amountOfHiddenLayers, size_t sizesOfLayers[]) {
    destroy();

    this->amountOfS = amountOfS;
    this->amountOfR = amountOfR;
    this->amountOfLayers = amountOfHiddenLayers + 1;

    layers = new Layer[amountOfLayers];
    actFuncType = new char[amountOfLayers];
    memset(actFuncType, 0, amountOfLayers * sizeof(char));

    size_t lastLayer = amountOfLayers - 1;
    for (size_t i = 0; i < amountOfLayers; i++) {
        if (i == 0)
            layers[i].initialize(amountOfS,            sizesOfLayers[i]);
        else if (i == lastLayer)
            layers[i].initialize(sizesOfLayers[i - 1], amountOfR);
        else
            layers[i].initialize(sizesOfLayers[i - 1], sizesOfLayers[i]);
    }
    input = new float[amountOfS];
}

void MultiLayerPerceptron::destroy() {
    if (layers) {
        delete[] layers;
        layers = NULL;
    }
    if (actFuncType) {
        delete[] actFuncType;
        actFuncType = NULL;
    }
    if (input) {
        delete[] input;
        input = NULL;
    }
}

MultiLayerPerceptron& MultiLayerPerceptron::operator = (const MultiLayerPerceptron& p) {
    initialize(p);
    return *this;
}

void MultiLayerPerceptron::restoreMultiLayerPerceptron(const MultiLayerPerceptronData& pd) {
    amountOfLayers = pd.amountOfLayers;

    size_t lastLayer = amountOfLayers - 1;
    amountOfS = pd.layers[0].amountOfInputNeurons;
    amountOfR = pd.layers[lastLayer].amountOfOutputNeurons;

    layers = new Layer[amountOfLayers];
    for (size_t i = 0; i < amountOfLayers; i++)
        layers[i].restoreLayer(pd.layers[i]);

    actFuncType = new char[amountOfLayers];
    memcpy(actFuncType, pd.actFuncType, amountOfLayers * sizeof(char));

    input = new float[amountOfS];
}

MultiLayerPerceptronData MultiLayerPerceptron::getMultiLayerPerceptronData() const {
    MultiLayerPerceptronData pd;
    pd.amountOfLayers = amountOfLayers;
    pd.layers = new LayerData[amountOfLayers];
    for (size_t i = 0; i < amountOfLayers; i++)
        pd.layers[i] = layers[i].getLayerData();
    pd.actFuncType = new char[amountOfLayers];
    memcpy(pd.actFuncType, actFuncType, amountOfLayers * sizeof(char));
    return pd;
}

void MultiLayerPerceptron::setInput(float in[]) {
    memcpy(input, in, amountOfS * sizeof(float));
}

void MultiLayerPerceptron::setInput(size_t index, float value) {
    if (index >= amountOfS)
        cerr << "Выход за границу массива в функции MultiLayerPerceptron::setInput()\n";
    input[index] = value;
}

void MultiLayerPerceptron::setActivationFunctionType(size_t indexOfLayer, char type) {
    actFuncType[indexOfLayer] = type;
}

void MultiLayerPerceptron::calculateOutput() {
    for (size_t k = 0; k < amountOfLayers; ++k) {
        float* in;
        if (k == 0)
            in = input;
        else
            in = layers[k - 1].output;

        #pragma omp parallel for
        for (size_t i = 0; i < layers[k].amountOfOutputNeurons; ++i)
            layers[k].sum[i] = dotProduct(layers[k].amountOfInputNeurons, layers[k].weights + i * layers[k].amountOfInputNeurons, in);

        actFunc(k);
    }
}

void MultiLayerPerceptron::backpropagation(float desierdOutput[]) {
    size_t lastLayer = amountOfLayers - 1;

    for (size_t i = 0; i < amountOfR; i++)
        layers[lastLayer].errors[i] = (getOutput(i) - desierdOutput[i]);
    actFuncDeriv(lastLayer);

    for (size_t k = lastLayer; k > 0; k--) {
        memset(layers[k - 1].errors, 0, layers[k - 1].amountOfOutputNeurons * sizeof(float));

        for (size_t i = 0; i < layers[k].amountOfOutputNeurons; i++)
            axpy(layers[k].amountOfInputNeurons, layers[k].weights + i * layers[k].amountOfInputNeurons, layers[k - 1].errors, layers[k].errors[i]);
        actFuncDeriv(k - 1);
    }
}

void MultiLayerPerceptron::teach(float desierdOutput[], float speed) {
    backpropagation(desierdOutput);

    for (size_t k = 0; k < amountOfLayers; k++) {
        float* in;
        if (k == 0)
            in = input;
        else
            in = layers[k - 1].output;

        for (size_t i = 0, index = 0; i < layers[k].amountOfOutputNeurons; i++)
            axpy(layers[k].amountOfInputNeurons, in, layers[k].weights + i * layers[k].amountOfInputNeurons, -speed * layers[k].errors[i]);
    }
}

void MultiLayerPerceptron::getDirection(Direction& dir) {
    size_t dirSize = 0;
    for (size_t i = 0; i < amountOfLayers; i++)
        dirSize += layers[i].amountOfInputNeurons * layers[i].amountOfOutputNeurons;

    dir.initialize(dirSize);
    for (size_t k = 0, index = 0; k < amountOfLayers; k++) {
        float* in;
        if (k == 0)
            in = input;
        else
            in = layers[k - 1].output;

        for (size_t i = 0; i < layers[k].amountOfOutputNeurons; i++)
            axpy(layers[k].amountOfInputNeurons, in, dir.direction + i * layers[k].amountOfInputNeurons, -layers[k].errors[i]);
    }
}

void MultiLayerPerceptron::moveTowards(Direction& dir, float length) {
    for (size_t k = 0, t = 0; k < amountOfLayers; k++)
        for (size_t i = 0, index = 0; i < layers[k].amountOfOutputNeurons; i++)
            for (size_t j = 0; j < layers[k].amountOfInputNeurons; j++, t++, index++)
                layers[k].weights[index] += dir[t] * length;

}

float* MultiLayerPerceptron::getOutput() const {
    float* out = new float[amountOfR];
    size_t lastLayer = amountOfLayers - 1;
    memcpy(out, layers[lastLayer].output, amountOfR * sizeof(float));
    return out;
}

float MultiLayerPerceptron::getOutput(size_t index) const {
    size_t lastLayer = amountOfLayers - 1;
    if (index >= amountOfR)
        cerr << "Выход за границу массива в функции MultiLayerPerceptron::getOutput()\n";
    return layers[lastLayer].output[index];
}

size_t MultiLayerPerceptron::getAmountOfR() const {
    return amountOfR;
}

size_t MultiLayerPerceptron::getAmountOfS() const {
    return amountOfS;
}

void MultiLayerPerceptron::actFunc(size_t indexOfLayer) {
    Layer& layer = layers[indexOfLayer];

    #pragma omp parallel for
    for (size_t i = 0; i < layer.amountOfOutputNeurons; i++) {
        switch(actFuncType[indexOfLayer]) {
        case 0:
            if (layer.sum[i] > 7)
                layer.output[i] = 1;
            else if (layer.sum[i] < -7)
                layer.output[i] = 0;
            else
                layer.output[i] = 1 / (1 + exp(-1 * layer.sum[i]));
            break;
        case 1:
            if (layer.sum[i] > 7)
                layer.output[i] = 1;
            else if (layer.sum[i] < -7)
                layer.output[i] = -1;
            else
                layer.output[i] = tanh(layer.sum[i]);
            break;
        case 2:
            if (layer.sum[i] > 7)
                layer.output[i] = 1.7159;
            else if (layer.sum[i] < -7)
                layer.output[i] = -1.7159;
            else
                layer.output[i] = 1.7159 * tanh(2.0f / 3.0f * layer.sum[i]);
            break;
        }
    }
}

void MultiLayerPerceptron::actFuncDeriv(size_t indexOfLayer) {
    Layer& layer = layers[indexOfLayer];
    for (size_t i = 0; i < layer.amountOfOutputNeurons; i++) {
        switch(actFuncType[indexOfLayer]) {
        case 0:
            if (abs(layer.sum[i]) > 7)
                layer.errors[i] = 0;
            else
                layer.errors[i] *= layer.output[i] * (1 - layer.output[i]);
            break;
        case 1:
            if (abs(layer.sum[i]) > 7)
                layer.errors[i] = 0;
            else {
                float ch = cosh(layer.sum[i]);
                layer.errors[i] *= 1 / ch / ch;
            }
            break;
        case 2:
            if (abs(layer.sum[i]) > 7)
                layer.errors[i] = 0;
            else {
                float ch = cosh(2.0f / 3.0f * layer.sum[i]);
                layer.errors[i] *= 1.7159 * 2 / 3 / ch / ch;
            }
            break;
        }
    }
}

MLP_Teacher::MLP_Teacher() {
    initialize();
}

void MLP_Teacher::initialize() {
    tests.clear();
    currentTest = 0;
    tail = 0;
    speed = 1;
    lastDir.initialize();
}

void MLP_Teacher::setMLP(const MultiLayerPerceptron& MLP) {
    currentTest = 0;
    tail = 0;
    speed = 1;
    perceptron = MLP;
    lastDir.initialize();
}

void MLP_Teacher::setTests(const vector<test_t>& tests) {
    this->tests.clear();
    currentTest = 0;
    tail = 0;
    speed = 1;
    this->tests = tests;
    lastDir.initialize();
}

float MLP_Teacher::calculateError() {
    float error = 0;
    for (size_t i = 0; i < tests[currentTest].second.size(); i++)
        error += 0.5 * (tests[currentTest].second[i] - perceptron.getOutput(i)) * (tests[currentTest].second[i] - perceptron.getOutput(i));
    return error;
}

vector<float> MLP_Teacher::step(float eps) {
    for (size_t i = 0; i < tests[currentTest].first.size(); i++)
        perceptron.setInput(i, tests[currentTest].first[i]);

    float out[10];
    for (int i = 0; i < 10; i++)
        out[i] = tests[currentTest].second[i];

    perceptron.calculateOutput();

    float maxError = 0;
    vector<float> errors(tests[currentTest].second.size());
    for (size_t i = 0; i < tests[currentTest].second.size(); i++) {
        errors[i] = abs(tests[currentTest].second[i] - perceptron.getOutput(i));
        maxError = max(maxError, errors[i]);
    }



    if (maxError > eps) {
        Direction dir;
        perceptron.backpropagation(out);
        perceptron.getDirection(dir);

        float a, b, c, d;
        float len = 3;
        Direction dir2;
        float best, discriminant;

        int option = 1;

        switch(option) {
        case 0:
            dir += lastDir * 0.9;
            lastDir = dir;
            perceptron.moveTowards(dir, 0.05);
            break;
        case 1:

            d = calculateError();
            c = -dir.length();

            if (abs(c) < 0.0001) {
                break;
            }

            dir.normalize();

            perceptron.moveTowards(dir, len);
            perceptron.calculateOutput();
            perceptron.backpropagation(out);
            perceptron.getDirection(dir2);

            if (dir * dir2 > 0) {
                if (calculateError() > d)
                    perceptron.moveTowards(dir, -len);
                break;
            }

            a = (-(dir * dir2) - c) / len - 2 * (calculateError() - d - c * len) / (len * len);
            b = (-(dir * dir2) - c - a * len * len) / len;

            discriminant = b * b - 3 * a * c;
            if (discriminant < 0){
                if (calculateError() > d)
                    perceptron.moveTowards(dir, -len);
                break;
            }

            best = (-b + sqrt(discriminant)) / (3 * a);
            if (best < 0 || best > len) {
                perceptron.moveTowards(dir, -len);
                cout << 0 << endl;
            }
            else {
                perceptron.moveTowards(dir, best - len);
                cout << best << ' ' << a << ' ' << b << ' ' << c << ' ' << d << endl;
                break;
            }
        case 2:

            float len = dir.length();
            if (len < 0.0001)
                break;

            dir /= len;
            perceptron.moveTowards(dir, 0.2);
            break;

        };

        if ((int)tests.size() - (int)tail - (int)currentTest - 1 > 0) {
            swap(tests[(int)tests.size() - tail - 1], tests[currentTest]);
            tail++;
        }
        printf("%10d     ", tail);
    }
    else
        printf("OK             ");

    currentTest++;
    if (currentTest >= tests.size()) {
        currentTest = 0;
        tail = 0;
        random_shuffle(tests.begin(), tests.end());
    }

    return errors;
}

MultiLayerPerceptron MLP_Teacher::getMLP() {
    return perceptron;
}
