#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <iostream>

typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned short ushort;

using namespace std;

class IO {
public:
	void openIF(string fn);
	void openOF(string fn);
	void closeIF();
	void closeOF();
	void writebit(uchar b);
	void write(uchar* x, int s);
	template <class T>
	void writet(T x, int s){
        for (int i = s; i > 0; i--){
            T q = 1;
            writebit((x & (q << (i - 1))) != 0);
        }
	}
	template <class T>
	void writet(T x) {
        for (int i = 0; i < sizeof(T); i++) {
            uchar* begin = ((uchar*)&x) + i;
            for (int j = 0; j < 8; j++)
                writebit(((*begin) & (1 << j)) != 0);
        }
	}
	bool readbit(uchar& b);
	bool read(uchar* x, int s);
	template <class T>
	bool readt(T& x, int s){
        bool flag = true;
        x = 0;
        for (int i = s; i > 0; i--){
            uchar b;
            flag = flag && readbit(b);
            T q = b;
            x |= q << (i - 1);
        }
        return flag;
	}
	template <class T>
	void readt(T& x) {
        for (int i = 0; i < sizeof(T); i++) {
            uchar* begin = ((uchar*)&x) + i;
            *begin = 0;

            for (int j = 0; j < 8; j++) {
                uchar ch;
                readbit(ch);
                if (ch)
                    (*begin) |= (1 << j);
            }
        }
	}
	void readFile(vector<uchar>& s);
	~IO();

private:
	ifstream in;
	ofstream out;
	uchar oc, ic;
	uint ot, it;
};

