AR=ar
ASM=nasm

dr:	libdot.a perceptron.h perceptron.cpp main.cpp IO/IO.h IO/IO.cpp dot_product.h
	g++ -O2 -g -pg -o dr  perceptron.h perceptron.cpp main.cpp IO/IO.h IO/IO.cpp dot_product.h -fopenmp -L. -ldot

libdot.a: dot_product.h dot_product.asm
	nasm -o dot_product.o -f elf64 dot_product.asm
	ar rcs $@ dot_product.o
	


