#include <iostream>
#include <cstdlib>
#include <fstream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <omp.h>
#include <sstream>

#include "IO/IO.h"
#include "perceptron.h"

using namespace std;

const int M = 28;
const int N = 28 * 28;

const int c = 1;
ElementaryPerceptron perceptron;
MultiLayerPerceptron mlp;
vector< pair< int, vector<int> > > tests;

void SaveInformation(string FileName) {
    SaveElementaryPerceptron(perceptron.getElementaryPerceptronData(), FileName);
}

void LoadInformation(string FileName) {
    ElementaryPerceptronData pd;
    LoadElementaryPerceptron(pd, FileName);
    perceptron.restoreElementaryPerceptron(pd);
}

void ReadTests(int amount = -1) {
    freopen("train.csv", "r", stdin);
    string s;
    cin >> s;

    int k = 0;
    while (cin >> s){
        if (k == amount) break;
        k++;

        s += ",";

        int digit = s[0] - '0';
        int ptr = 1;

        vector<int> image;
        for (int j = 0; j < N; j++){
            int x = 0;

            ptr++;
            while (s[ptr] != ','){
                x = x * 10 + s[ptr] - '0';
                ptr++;
            }

            image.push_back(x);
        }
        tests.push_back(make_pair(digit, image));
    }
}

void SaveTests() {
    ofstream out("train.csv");

    out << "label";
    for (int i = 0; i < N; i++)
        out << ",pixel" << i;
    out << endl;

    for (int i = 0; i < tests.size(); i++) {
        out << tests[i].first;
        for (int j = 0; j < N; j++)
            out << "," << tests[i].second[j];
        out << endl;
    }

    out.close();
}

void SaveAOutput() {
    LoadInformation("data1.txt");
    IO io;
    io.openOF("AOutput.txt");
    ReadTests();

    for (size_t i = 0; i < tests.size(); i++) {
        printf("%d      %d\n", i, tests[i].first);

        #pragma omp parallel for
        for (int j = 0; j < N; j++)
            perceptron.setInput(j, tests[i].second[j] > 50);
        perceptron.calculateAOutput();

        int amountOfA = perceptron.getAmountOfA();
        for (int j = 0; j < amountOfA; j++)
            io.writebit((uchar)perceptron.getAOutput(j));
    }
    io.closeOF();
}

void Teach(int iterations) {
    LoadInformation("data1.txt");
    ReadTests();

    for (int it = 0; it < iterations; it++) {
        IO io;
        io.openIF("AOutput.txt");
        for (size_t i = 0; i < tests.size(); i++) {
            printf("%d      %d      %d      ", it, i, tests[i].first);

            int amountOfA = perceptron.getAmountOfA();
            for (int j = 0; j < amountOfA; j++) {
                uchar value;
                io.readbit(value);
                perceptron.setAOutput(j, value);
            }

            perceptron.calculateROutput();

            int desierdOutput[10];
            for (int j = 0; j < 10; j++)
                if (tests[i].first == j)
                    desierdOutput[j] = 1;
                else
                    desierdOutput[j] = -1;
            perceptron.teach(desierdOutput);

            for (int j = 0; j < 10; j++) {
                bool ans1 = tests[i].first == j;
                bool ans2 = perceptron.getROutput(j) > 0;
                printf("%d", (int)(ans1 == ans2));
            }
            printf("\n");
        }
        SaveInformation("data1.txt");
        io.closeIF();
    }
}

double GetError() {
    LoadInformation("data1.txt");
    ReadTests();
    IO io;
    io.openIF("AOutput.txt");

    int mistakes = 0;
    for (size_t i = 0; i < tests.size(); i++) {

        printf("%d\n", i);

        int amountOfA = perceptron.getAmountOfA();
        for (int j = 0; j < amountOfA; j++) {
            uchar value;
            io.readbit(value);
            perceptron.setAOutput(j, value);
        }
        perceptron.calculateROutput();

        int ans = 0;
        for (int j = 0; j < 10; j++)
            if (perceptron.getROutput(j) > perceptron.getROutput(ans)) ans = j;
        if (ans != tests[i].first)
            mistakes++;
    }

    return (double)mistakes / tests.size();
}

void MakeBMP(vector< vector<double> > matrix, int width, int height, string FileName) {
	ofstream fout(FileName.c_str(), std::ios::binary);

	unsigned char signature[2] = { 'B', 'M' };
	unsigned int fileSize = 14 + 40 + width*height*4;
	unsigned int reserved = 0;
	unsigned int offset = 14 + 40;

	unsigned int headerSize = 40;
	unsigned int dimensions[2] = { width, height };
	unsigned short colorPlanes = 1;
	unsigned short bpp = 32;
	unsigned int compression = 0;
	unsigned int imgSize = width * height * 4;
	unsigned int resolution[2] = { 2795, 2795 };
	unsigned int pltColors = 0;
	unsigned int impColors = 0;

	fout.write(reinterpret_cast<char*>(signature), sizeof(signature));
	fout.write(reinterpret_cast<char*>(&fileSize), sizeof(fileSize));
	fout.write(reinterpret_cast<char*>(&reserved), sizeof(reserved));
	fout.write(reinterpret_cast<char*>(&offset),   sizeof(offset));

	fout.write(reinterpret_cast<char*>(&headerSize),  sizeof(headerSize));
	fout.write(reinterpret_cast<char*>(dimensions),   sizeof(dimensions));
	fout.write(reinterpret_cast<char*>(&colorPlanes), sizeof(colorPlanes));
	fout.write(reinterpret_cast<char*>(&bpp),         sizeof(bpp));
	fout.write(reinterpret_cast<char*>(&compression), sizeof(compression));
	fout.write(reinterpret_cast<char*>(&imgSize),     sizeof(imgSize));
	fout.write(reinterpret_cast<char*>(resolution),   sizeof(resolution));
	fout.write(reinterpret_cast<char*>(&pltColors),   sizeof(pltColors));
	fout.write(reinterpret_cast<char*>(&impColors),   sizeof(impColors));

	unsigned char x, r, g, b;
	for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            x = 0;
            r = matrix[height - i - 1][j] * 255;
            g = matrix[height - i - 1][j] * 255;
            b = 100;//matrix[i][j] * 255;
            fout.write(reinterpret_cast<char*>(&b),sizeof(b));
            fout.write(reinterpret_cast<char*>(&g),sizeof(g));
            fout.write(reinterpret_cast<char*>(&r),sizeof(r));
            fout.write(reinterpret_cast<char*>(&x),sizeof(x));
        }
	}

	fout.close();
}

void MakeAnswer() {
    LoadInformation("data1.txt");

    vector< vector<int> > images;

    freopen("test.csv", "r", stdin);
    ofstream fout("answer.txt");
    string s;
    cin >> s;
    while (cin >> s){
        s += ",";

        int ptr = 0;

        vector<int> image;
        for (int j = 0; j < N; j++){
            int x = 0;

            while (s[ptr] != ','){
                x = x * 10 + s[ptr] - '0';
                ptr++;
            }
            ptr++;

            image.push_back(x);
        }
        images.push_back(image);
    }

    for (int i = 0; i < images.size(); i++) {
        printf("%d\n", i);
        for (int j = 0; j < N; j++)
            perceptron.setInput(j, images[i][j] > 50);
        perceptron.calculateOutput();
        int ans = 0;
        for (int j = 0; j < perceptron.getAmountOfR(); j++)
            if (perceptron.getROutput(ans) < perceptron.getROutput(j)) ans = j;
        fout << ans << endl;
    }
}

void MLP_SaveInformation(string FileName) {
    SaveMultiLayerPerceptron(mlp.getMultiLayerPerceptronData(), FileName);
}

void MLP_LoadInformation(string FileName) {
    MultiLayerPerceptronData pd;
    LoadMultiLayerPerceptron(pd, FileName);
    mlp.restoreMultiLayerPerceptron(pd);
}

double MLP_GetError() {
    MLP_LoadInformation("data.txt");
    ReadTests();

    int mistakes = 0;
    for (size_t i = 0; i < tests.size(); i++) {

        printf("%d\n", i);

        float input[N];
        for (size_t j = 0; j < N; j++)
            input[j] = tests[i].second[j] / 256.0;
        mlp.setInput(input);
        mlp.calculateOutput();

        int ans = 0;
        for (int j = 0; j < 10; j++)
            if (mlp.getOutput(j) > mlp.getOutput(ans)) ans = j;
        if (ans != tests[i].first)
            mistakes++;
    }

    return (double)mistakes / (tests.size());
}

void MLP_MakeAnswer() {
    MLP_LoadInformation("data.txt");

    vector< vector<int> > images;

    freopen("test.csv", "r", stdin);
    ofstream fout("answer.txt");
    string s;
    cin >> s;
    while (cin >> s){
        s += ",";

        int ptr = 0;

        vector<int> image;
        for (int j = 0; j < N; j++){
            int x = 0;

            while (s[ptr] != ','){
                x = x * 10 + s[ptr] - '0';
                ptr++;
            }
            ptr++;

            image.push_back(x);
        }
        images.push_back(image);
    }

    for (int i = 0; i < images.size(); i++) {
        printf("%d\n", i);
        for (int j = 0; j < N; j++)
            mlp.setInput(j, images[i][j] / 256.0);
        mlp.calculateOutput();
        int ans = 0;
        for (int j = 0; j < mlp.getAmountOfR(); j++)
            if (mlp.getOutput(ans) < mlp.getOutput(j)) ans = j;
        fout << ans << endl;
    }
}

void ShiftDigits() {
    int Size = tests.size();
    for (int k = 0; k < Size; k++) {
        vector<int> in[4];

        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                if (i < 2)
                    in[0].push_back(0);
                else
                    in[0].push_back(tests[k].second[(i - 2) * M + j]);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                if (i >= M - 2)
                    in[1].push_back(0);
                else
                    in[1].push_back(tests[k].second[(i + 2) * M + j]);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                if (j < 2)
                    in[2].push_back(0);
                else
                    in[2].push_back(tests[k].second[i * M + (j - 2)]);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                if (j >= M - 2)
                    in[3].push_back(0);
                else
                    in[3].push_back(tests[k].second[i * M + (j + 2)]);
        tests.push_back(make_pair(tests[k].first, in[rand() % 4]));
    }
}

void WarpDigits() {
    const int S = 19;
    const float sigma = 4;
    int c = (S - 1) / 2;

    float divisor = 0;
    vector< vector<double> > core(S);
    for (int i = 0; i < S; i++)
        for (int j = 0; j < S; j++) {
            float d = (i - c) * (i - c) + (j - c) * (j - c);
            core[i].push_back(exp(-d / (2 * sigma * sigma)));
            divisor += core[i][j];
        }
    MakeBMP(core, S, S, "core.bmp");
    for (int i = 0; i < S; i++)
        for (int j = 0; j < S; j++)
            core[i][j] /= divisor;


    int Size = tests.size();
    for (int k = 0; k < Size; k++) {
        cout << k << endl;

        vector< vector<double> > randomMatrix[2];
        vector< vector<double> > deltha[2];

        int t = M + c + c;
        for (int dim = 0; dim < 2; dim++) {
            randomMatrix[dim].resize(t);
            for (int i = 0; i < t; i++) {
                randomMatrix[dim][i].resize(t);
                for (int j = 0; j < t; j++)
                    randomMatrix[dim][i][j] = ((1.0 * rand() / RAND_MAX - 0.5) * 60);
            }

            deltha[dim].resize(M);
            for (int x = c; x < M + c; x++) {
                deltha[dim][x - c].resize(M);
                for (int y = c; y < M + c; y++) {
                    float sum = 0;
                    for (int dx = -c; dx <= c; dx++)
                        for (int dy = -c; dy <= c; dy++)
                            sum += randomMatrix[dim][x + dx][y + dy] * core[dx + c][dy + c];
                    deltha[dim][x - c][y - c] = sum;
                }
            }
            /*cout.precision(2);
            cout << fixed;
            for (int i = 0; i < M; i++) {
                for (int j = 0; j < M; j++) {
                    cout << deltha[dim][i][j] << ' ';
                    //deltha[dim][i][j] = (deltha[dim][i][j] + 1) / 2;
                }
                cout << endl;
            }
            MakeBMP(deltha[dim], M, M, "tmp.bmp");*/
        }

        //vector< vector<double> > im1(M);
        //vector< vector<double> > im2(M);
        vector<int> newImage(N);
        for (int x = 0; x < M; x++)
            for (int y = 0; y < M; y++) {
                float fx = x + deltha[0][x][y];
                float fy = y + deltha[1][x][y];

                if (fx < 0) fx = 0;
                if (fx > M - 1.1) fx = M - 1.1;
                if (fy < 0) fy = 0;
                if (fy > M - 1.1) fy = M - 1.1;

                int nx = fx;
                int ny = fy;
                float dx = fx - nx;
                float dy = fy - ny;

                vector<int>& im = tests[k].second;
                float y1 = (im[(nx + 1) * M + ny] - im[nx * M + ny]) * dx + im[nx * M + ny];
                float y2 = (im[(nx + 1) * M + (ny + 1)] - im[nx * M + (ny + 1)]) * dx + im[nx * M + (ny + 1)];
                newImage[x * M + y] = (y2 - y1) * dy + y1;
                //im1[x].push_back(newImage[x * M + y] / 255.0);
                //im2[x].push_back(im[x * M + y] / 255.0);
            }

        tests.push_back(make_pair(tests[k].first, newImage));

        /*ostringstream out1;
        out1 << "digits/" << k << "_new.bmp";
        ostringstream out2("digits/");
        out2 << "digits/" << k << "_old.bmp";
        MakeBMP(im1, M, M, out1.str().c_str());
        MakeBMP(im2, M, M, out2.str().c_str());*/
    }
}

int main(){

    srand(clock());

    /*perceptron = ElementaryPerceptron(N, 10000, 10);
    SaveInformation("data1.txt");
    LoadInformation("data1.txt");
    SaveAOutput();

    Teach(300);
    cout << GetError() << endl;
    MakeAnswer();

    return 0;*/


    cout.precision(5);
    cout << fixed;

    //MLP_MakeAnswer();
    //cout << MLP_GetError();
    //return 0;

    ReadTests(42000 * 4);
    //ReadTests(10000);

    //ShiftDigits();
    //WarpDigits();
    //SaveTests();
    //return 0;

    vector<test_t> MLP_Tests(tests.size());
    for (size_t i = 0; i < tests.size(); i++) {
        vector<float> input, output;
        for (size_t j = 0; j < tests[i].second.size(); j++)
            input.push_back(tests[i].second[j] / 256.0);
        for (int j = 0; j < 10; j++)
            if (tests[i].first == j)
                output.push_back(1);
            else
                output.push_back(0);
        MLP_Tests[i] = make_pair(input, output);
    }

    //mlp = MultiLayerPerceptron(N, 10, 1000);
    //MLP_SaveInformation("data.txt");
    MLP_LoadInformation("data.txt");

    MLP_Teacher teacher;
    teacher.setMLP(mlp);
    teacher.setTests(MLP_Tests);

    for (int i = 0; i < tests.size() * 100; i++) {
        printf("%5d  ", i / tests.size());
        if (i % 2000 == 0) {
            mlp = teacher.getMLP();
            MLP_SaveInformation("data.txt");
        }
        vector<float> errors = teacher.step(0.1);
        for (int j = 0; j < 10; j++)
            cout << (errors[j] < 0.49 ? 0 : 1);
        cout << endl;
    }
    mlp = teacher.getMLP();
    //MLP_SaveInformation("data.txt");

	return 0;
}
