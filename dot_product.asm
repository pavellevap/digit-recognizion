global dotProductSSE

dotProductSSE:
		
        xorps xmm0, xmm0
        
.loop
		test rdx, 0x3
		je .loop_end
		
		movss xmm1, [rsi + rdx*4 - 4]
		movss xmm2, [rdi + rdx*4 - 4]
		
		mulss xmm1, xmm2
		addss xmm0, xmm1
		
		sub rdx, 1
		jmp .loop
.loop_end

.main_loop
        test rdx, rdx
        je .main_loop_end

        movups xmm1, [rsi + rdx*4 - 16]
        movups xmm2, [rdi + rdx*4 - 16]

        mulps xmm1, xmm2
        addps xmm0, xmm1
        
        sub rdx, 4
        jmp .main_loop
.main_loop_end

        haddps xmm0, xmm0
        haddps xmm0, xmm0
        ret

        

        
         
