/**
Библиотека для работы с перцептронами
*/

#pragma once

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include <algorithm>
#include <omp.h>

#include "IO/IO.h"
#include "dot_product.h"

using std::string;
using std::cerr;
using std::endl;
using std::pair;
using std::max;
using std::min;
using std::swap;
using std::cout;
using std::ofstream;
using std::ifstream;

typedef pair< vector<float>, vector<float> > test_t;  /** Тест - пара из входного и выходного вектора */

/**
======================================================================================================
                                Элементарный перцептрон.
 https://ru.wikipedia.org/wiki/Перцептрон.
 Перцептрон представляет собой сеть нейронов.
 Нейроны разделены на 3 типа: S - сенсорные, A - ассоциативные, R - результатирущие.
 R нейроны - выходные значения перцепртона. S нейроны - входные значения перцептрона.
 Каждый А нейрон получеет сигнал от некоторых S нейронов и передает сигнал R нейронам.
 Веса S-A ребер - элементы {-1, 1}, выбираются случайно и не меняются.
 Пороговые значения A нейронов равны 0.
 Выход А-нейронов - 0 или 1. Активировался нейрон или нет.
 Выход R-нейронов - целое число. > 0 если вход относится к 1ому классу, < 0 если ко второму.
 Обучение по методу коррекции ошибки.

 Замечания:
 1) Для гарантированного решения задачи xor двух битов потребовалось 20 A-нейронов
 2) Из-за того, что пороговые значения равны 0, то на нулевом входе будет нулевой выход.
     Чтобы этого избежать можно добавить один фиктивный вход, который всегда будет активным.
 3) Есть возможность ускорить обучение за счет запоминания выходных значений A нейронов
     для каждого входа.
 4) R нейрон возвращает не {+1, -1} а произвольное целое число, причем если это число больше 0,
     то скорее всего вход принадлежит объекту 1ого класса, иначе объекту 2ого класса.
     чем больше модуль этого числа, тем больше уверенность перцептрона в выданном ответе.
======================================================================================================
*/

/**
@detailed Вся необходимая информация для восстановления перцептрона
          Значение этого типа возвращает метод ElementaryPerceptron::getElementaryPerceptronData()
          Перцептрон восстанавливается методом ElementaryPerceptron::restoreElementaryPerceptron(ElementaryPerceptronData)
*/
class ElementaryPerceptronData{
public:

    ElementaryPerceptronData();

    ElementaryPerceptronData(const ElementaryPerceptronData& pd);

    ~ElementaryPerceptronData();

    void initialize();

    void initialize(const ElementaryPerceptronData& pd);

    void destroy();

    ElementaryPerceptronData& operator = (const ElementaryPerceptronData& pd);

	size_t amountOfS;                    /** Количество S нейронов */
	size_t amountOfA;                    /** Количество A нейронов */
	size_t amountOfR;                    /** Количество R нейронов */

	short** ASEdges;                     /** Веса A-S ребер */
	short** RAEdges;                     /** Веса R-A ребер */
};

void SaveElementaryPerceptron(const ElementaryPerceptronData& pd, string FileName);

void LoadElementaryPerceptron(ElementaryPerceptronData& pd, string FileName);

/**
Реализует интерфейс элементарного перцептрона.
*/
class ElementaryPerceptron{
public:

	ElementaryPerceptron();

	/**
	@param amountOfS число входов
	@param amountOfA число нейронов скрытого слоя
	@param amountOfR число выходов
	*/
	ElementaryPerceptron(size_t amountOfS, size_t amountOfA, size_t amountOfR);

	ElementaryPerceptron(const ElementaryPerceptron& p);

	~ElementaryPerceptron();

	/**
	@brief Инициализация нулевыми значениями
	*/
	void initialize();

	/**
	@brief Инициализация существующим объектом
	*/
	void initialize(const ElementaryPerceptron& p);

    /**
    @brief Инициализация
	@param amountOfS число входов
	@param amountOfA число нейронов скрытого слоя
	@param amountOfR число выходов
	*/
	void initialize(size_t amountOfS, size_t amountOfA, size_t amountOfR);

    /**
    @brief Освобождает память
    */
	void destroy();

	ElementaryPerceptron& operator = (const ElementaryPerceptron& p);

	/**
    @brief Восстанавливает перцептрон
    @param pd Структура хранящая информацию о перцептроне
    */
	void restoreElementaryPerceptron(const ElementaryPerceptronData& pd);

	/**
    @brief Возвращает информацию необходимую для восстановления перцептрона
    */
    ElementaryPerceptronData getElementaryPerceptronData() const;

    /**
    @brief Устанавливает значения S нейронов
    @param in Массив входных значений S нейронов
    */
    void setInput(bool in[]);

	/**
    @brief Устанавливает значение S нейрона
    @param index Индекс S нейрона
    @param value Входное значение
    */
    void setInput(size_t index, bool value);

    /**
    @brief Устанавливает значения A нейронов
    @param out Массив входных значений A нейронов
    */
    void setAOutput(bool out[]);

	/**
    @brief Устанавливает значение A нейрона
    @param index Индекс A нейрона
    @param value Выходное значение
    */
    void setAOutput(size_t index, bool value);

    /**
    @brief Использовать только после ElementaryPerceptron::setInput()
    @brief Вычисляет значения A нейронов, и сохраняет значение в закрытой переменной класса.
    @brief Чтобы получить значение используйте ElementaryPerceptron::getAOutput().
    */
	void calculateAOutput();

	/**
    @brief Использовать только после ElementaryPerceptron::calculateAOutput() или ElementaryPerceptron::setAOutput()
    @brief Вычисляет значения R нейронов, и сохраняет значение в закрытой переменной класса.
    @brief Чтобы получить значение используйте ElementaryPerceptron::getROutput().
    */
	void calculateROutput();

	/**
    @brief Использовать только после ElementaryPerceptron::setInput()
    @brief Вычисляет значения A и R нейронов, и сохраняет значения в закрытой переменной класса.
    @brief Чтобы получить значение используйте ElementaryPerceptron::getROutput() или ElementaryPerceptron::getAOutput().
    */
	void calculateOutput();

	/**
    @brief Корректирует веса ребер, ведущих от A нейронов к R_index нейрону
    @param index номер R нейрона
    @param add величина на которую изменятся веса ребер
    */
	void correct(size_t index, int add);

	/**
	@brief Использовать тоько после ElementaryPerceptron::calculateROutput() или ElementaryPerceptron::calculateOutput().
	@brief Корректирует веса R-A связей так чтобы в будущем ответ перцептрона
    @brief был больше похож на desierdOutput.
    @param desierdOutput Желаемый выход перцептрона.
	*/
	void teach(int desierdOutput[]);

	/**
    @brief Использовать только после ElementaryPerceptron::calculateAOutput()
    @brief Возвращает выходные значения A нейронов.
    @brief Возвращаемый методом массив, создан с помощью new. Не забывайте его удалять!!!
    */
    bool* getAOutput() const;

    /**
    @brief Использовать только после ElementaryPerceptron::calculateAOutput()
    @brief Возвращает выходное значение A нейрона.
    */
    bool getAOutput(size_t index) const;

	/**
    @brief Использовать только после ElementaryPerceptron::calculateROutput()
    @brief Возвращает выходные значения R нейронов.
    @brief Возвращаемый методом массив, создан с помощью new. Не забывайте его удалять!!!
    */
    int* getROutput() const;

	/**
    @brief Использовать только после ElementaryPerceptron::calculateROutput()
    @brief Возвращает выходное значение R нейрона.
    */
    int getROutput(size_t index) const;

	/**
	@brief Возвращает кол-во R-нейронов
	*/
	size_t getAmountOfR() const;

	/**
	@brief Возвращает кол-во A-нейронов
	*/
	size_t getAmountOfA() const;

	/**
	@brief Возвращает кол-во S-нейронов
	*/
	size_t getAmountOfS() const;

private:
	size_t  amountOfS;                    /** Количество S нейронов */
	size_t  amountOfA;                    /** Количество A нейронов */
	size_t  amountOfR;                    /** Количество R нейронов */

	bool*   input;                        /** Входные значения для S нейронов. 0 или 1 */

	short** ASEdges;                      /** Веса A-S ребер */
	bool*   AOutput;                      /** Выход A нейронов */
	short** RAEdges;                      /** Веса R-A ребер */
	int*    ROutput;                      /** Выход R нейронов */
};


/**
======================================================================================================
                                Многослойный перцептрон
    Отличия от элементарного перцептрона:
     1) Скрытых слоев может быть произвольное число.
     2) Веса связей имеют тип float.
     3) Функция активации диффиренцируема.
     4) Обучаются все слои.

    Нахождение градиента встроено в класс многослойного перцептрона.
    Для каждого слоя есть возможность выбрать свою функцию активации.
    Для обучения перцептрона есть специальный класс. Можно выбирать способ обучения.
======================================================================================================
*/

float dotProduct(const size_t N, const float *X, const float* Y);

void axpy(const size_t N, const float *X, float* Y, float a);   /// y = ax + y

/**
@detailed Вся необходимая информация для восстановления одного слоя перцептрона
*/
class LayerData {
public:

    LayerData();

    LayerData(const LayerData& ld);

	~LayerData();

	LayerData& operator = (const LayerData& ld);

    /**
	@brief Инициализация нулевыми значениями
	*/
	void initialize();

	/**
	@brief Инициализация существующим объектом
	*/
	void initialize(const LayerData& ld);

    /**
    @brief Освобождает память
    */
	void destroy();


	size_t  amountOfInputNeurons;               /// Число входных нейронов слоя
	size_t  amountOfOutputNeurons;              /// Число выходных нейронов слоя

	float* weights;                            /// Матрица весов слоя. weights[i][j] - вес связи i-ого выходного нейрона
                                                /// и jого входного нейрона
};

/**
@detailed Вся необходимая информация для восстановления перцептрона
*/
class MultiLayerPerceptronData {
public:

    MultiLayerPerceptronData();

    MultiLayerPerceptronData(const MultiLayerPerceptronData& pd);

	~MultiLayerPerceptronData();

    /**
    @brief Инициализация нулевыми значениями
    */
	void initialize();

    /**
    @brief Инициализация существующим объектом
    */
	void initialize(const MultiLayerPerceptronData& pd);

    /**
    @brief Освобождает память
    */
	void destroy();

	MultiLayerPerceptronData& operator = (const MultiLayerPerceptronData& pd);

	size_t     amountOfLayers;          /// Число слоев перцептрона
	LayerData* layers;                  /// Информация о слоях
	char*      actFuncType;             /// Тип функции активации для каждого слоя
};

void SaveMultiLayerPerceptron(const MultiLayerPerceptronData& pd, string FileName);

void LoadMultiLayerPerceptron(MultiLayerPerceptronData& pd, string FileName);

/**
@brief Слой многослойного перцептрона.
@brief Предназначен для хранения информации о слое.
@brief Не производит вычислений.
*/
class Layer {
public:

    Layer();

    Layer(const Layer& l);

    /**
    @param amountOfInputs число входных нейронов слоя
    @param amountOfOutputs число выходных нейронов слоя
    */
    Layer(size_t amountOfInputs, size_t amountOfOutputs);

    ~Layer();

    Layer& operator = (const Layer& l);

    /**
    @brief Инициализация нулевыми значениями
    */
    void initialize();

    /**
    @brief Инициализация существующим объектом
    */
    void initialize(const Layer& l);

    /**
    @brief Инициализация случайными значениями из диапозона (-0.5; 0.5)
    @param amountOfInputs число входных нейронов слоя
    @param amountOfOutputs число выходных нейронов слоя
    */
    void initialize(size_t amountOfInputs, size_t amountOfOutputs);

    /**
    @brief Освобождает память
    */
    void destroy();

    /**
    @brief Восстанавливает слой
    @param ld Информация о слое
    */
    void restoreLayer(const LayerData& ld);

    /**
    @brief Возвращает информацию о слое
    @return Информация о слое
    */
    LayerData getLayerData() const;

    friend class MultiLayerPerceptron;

private:
    float*  weights;                    /** Веса связей */

    float*  sum;                        /** Суммарный сигнал поступивший в нейрон */
    float*  output;                     /** Выходные значания */
    float*  errors;                     /** Ошибки выходных нейронов */

    size_t  amountOfInputNeurons;       /** Число входных нейронов */
    size_t  amountOfOutputNeurons;      /** Число выходных нейронов */
};

class Direction {
public:
    Direction();
    Direction(size_t size);
    Direction(const Direction& t);
    ~Direction();
    Direction& operator = (const Direction& t);
    void initialize();
    void initialize(size_t size);
    void initialize(const Direction& t);
    void destroy();

    void normalize();

    float length();

    void randomDirection();

    float& operator [] (size_t index);

    float operator [] (size_t index) const;

    friend float operator * (const Direction& dir1, const Direction& dir2);

    friend Direction operator + (const Direction& dir1, const Direction& dir2);

    friend Direction operator - (const Direction& dir1, const Direction& dir2);

    friend Direction operator * (const Direction& dir, float x);

    friend Direction operator * (float x, const Direction& dir);

    friend Direction operator / (const Direction& dir, float x);

    friend Direction& operator += (Direction& dir1, const Direction& dir2);

    friend Direction& operator -= (Direction& dir1, const Direction& dir2);

    friend Direction& operator *= (Direction& dir, float x);

    friend Direction& operator /= (Direction& dir, float x);

    friend class MultiLayerPerceptron;

private:
    size_t size;
    float* direction;
};

/**
Реализует интерфейс многослойного перцептрона.
*/
class MultiLayerPerceptron {
public:

	MultiLayerPerceptron();

	MultiLayerPerceptron(const MultiLayerPerceptron& p);

	/**
    @brief Срздает перцептрон с amountOfS входами, amountOfR выходами и
    @brief единственным скрытым слоем с кол-вом A нейронов равным amountOfA
    */
	MultiLayerPerceptron(size_t amountOfS, size_t amountOfR, size_t amountOfA);

	/**
    @brief Срздает перцептрон с amountOfS входами, amountOfR выходами и двумя скрытыми слоями
    @brief с кол-вом A нейронов равными amountOfAInFirstLayer и amountOfAInSecondLayer
    */
	MultiLayerPerceptron(size_t amountOfS, size_t amountOfR,
                         size_t amountOfAInFirstLayer, size_t  amountOfAInSecondLayer);

	/**
	@brief Срздает перцептрон с amountOfS входами, amountOfR выходами и amountOfHiddenLayers скрытыми слоями.
    @brief Размеры слоев берутся из массива sizesOfLayers
	*/
	MultiLayerPerceptron(size_t amountOfS, size_t amountOfR,
                         size_t amountOfHiddenLayers, size_t sizesOfLayers[]);

    ~MultiLayerPerceptron();

    void initialize();

    void initialize(const MultiLayerPerceptron& p);

    /**
    @brief Инициализирует перцептрон
    */
	void initialize(size_t amountOfS, size_t amountOfR,
	                size_t amountOfHiddenLayers, size_t sizesOfLayers[]);

    void destroy();

    MultiLayerPerceptron& operator = (const MultiLayerPerceptron& p);

	/**
    @brief Восстанавливает перцептрон
    @param pd Структура хранящая информацию о перцептроне
    */
	void restoreMultiLayerPerceptron(const MultiLayerPerceptronData& pd);

	/**
    @brief Возвращает информацию необходимую для восстановления перцептрона
    */
    MultiLayerPerceptronData getMultiLayerPerceptronData() const;

    /**
    @brief Устанавливает значения S нейронов
    @param in Массив входных значений S нейронов. Желателено в диапозоне от 0 до 1.
    */
    void setInput(float in[]);

	/**
    @brief Устанавливает значение S нейрона
    @param index Индекс S нейрона
    @param value Входное значение. Желательно в диапозоне от 0 до 1.
    */
    void setInput(size_t index, float value);

    /**
    @brief Устанавливает функцию type в качестве функции активации для слоя с номером indexOfLayer.
    */
    void setActivationFunctionType(size_t indexOfLayer, char type);

	/**
    @brief Использовать только после MultiLayerPerceptron::setInput()
    @brief Вычисляет значения A и R нейронов, и сохраняет значения в закрытой переменной класса.
    @brief Чтобы получить значение используйте MultiLayerPerceptron::getROutput() или MultiLayerPerceptron::getAOutput().
    */
	void calculateOutput();

	/**                         Лучше сделать приватным
	@brief Использовать тоько после MultiLayerPerceptron::calculateOutput().
	@brief Вычисляет ошибки нйронов.
    @param desierdOutput Желаемый выход перцептрона.
    */
	void backpropagation(float desierdOutput[]);

	/**
	@brief Использовать тоько после MultiLayerPerceptron::calculateOutput().
	@brief Корректирует веса связей так чтобы в будущем ответ перцептрона
    @brief был больше похож на desierdOutput.
    @param desierdOutput Желаемый выход перцептрона.
	*/
	void teach(float desierdOutput[], float speed = 1);

	/**
	@brief Возвращает единичный вектор, противоположный направлению градиента
	@brief Вызывать после backpropagation()
    @param dir направление
    */
	void getDirection(Direction& dir);

	/**
	@brief Изменяет веса на вектор dir * length
	@param dir направление
	@param length длина
	*/
	void moveTowards(Direction& dir, float length);

	/**
    @brief Использовать только после MultiLayerPerceptron::calculateOutput()
    @brief Возвращает выходные значения R нейронов.
    @brief Возвращаемый методом массив, создан с помощью new. Не забывайте его удалять!!!
    */
    float* getOutput() const;

	/**
    @brief Использовать только после MultiLayerPerceptron::calculateOutput()
    @brief Возвращает выходное значение R нейрона.
    */
    float getOutput(size_t index) const;

	/**
	@brief Возвращает кол-во R-нейронов
	*/
	size_t getAmountOfR() const;

	/**
	@brief Возвращает кол-во S-нейронов
	*/
	size_t getAmountOfS() const;

	friend class MLP_Teacher;

private:
	size_t   amountOfS;             /** Количество S нейронов */
	size_t   amountOfR;             /** Количество R нейронов */

    size_t   amountOfLayers;        /** Количество слоев связей, т.е. колличество скрытых слоев + выходной слой */

	float*   input;                 /** Входные значения для S нейронов. Желательно в диапозоне от 0 до 1 */

	Layer*   layers;                /** Слои перцептрона */
	char*    actFuncType;           /** Тип функции активации для каждого слоя */

	void actFunc(size_t indexOfLayer);         /** Расчитывает выходные значения всех нейронов слоя */

	void actFuncDeriv(size_t indexOfLayer);    /** Расчитывает выходные значения всех нейронов слоя при обратном распространении */
};

class MLP_Teacher {
public:
    MLP_Teacher();
    void initialize();

    void setMLP(const MultiLayerPerceptron& MLP);
    void setTests(const vector<test_t>& tests);

    float calculateError();

    vector<float> step(float eps);

    MultiLayerPerceptron getMLP();
private:
    MultiLayerPerceptron perceptron;
    float speed;
    vector<test_t> tests;
    size_t currentTest;
    size_t tail;
    Direction lastDir;
};

